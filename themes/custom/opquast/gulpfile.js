var gulp        = require("gulp");
var compass     = require('gulp-compass');
var browserSync = require("browser-sync");
var reload = browserSync.reload;
var sourcemaps = require('gulp-sourcemaps');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var browserify = require('browserify');
var watchify = require('watchify');
var babel = require('babelify');
/*var connect = require('gulp-connect');*/

function compile(watch) {
  var bundler = watchify(browserify('js/app.js', { debug: true }).transform(babel.configure({
      // Use all of the ES2015 spec
      presets: ["es2015"]
    }))
  );

  function rebundle() {
    bundler.bundle()
      .on('error', function(err) { console.error(err); this.emit('end'); })
      .pipe(source('build.js'))
      .pipe(buffer())
      .pipe(sourcemaps.init({ loadMaps: true }))
      .pipe(sourcemaps.write('./'))
      .pipe(gulp.dest('./build'));
  }

  if (watch) {
    bundler.on('update', function() {
      console.log('-> bundling...');
      rebundle();
    });
  }

  rebundle();
}

function watch() {
  return compile(true);
}

function es6() {
  var bundler = watchify(browserify('js/app.js', { debug: true }).transform(babel.configure({
      // Use all of the ES2015 spec
      presets: ["es2015"]
    }))
  );

  bundler.bundle()
    .on('error', function(err) { console.error(err); this.emit('end'); })
    .pipe(source('build.js'))
    .pipe(buffer())
    .pipe(sourcemaps.init({ loadMaps: true }))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./build'))
    .once('end', function () {
      process.exit();
    });
}

gulp.task('es6', function() { return es6() });

gulp.task('build', function() { return compile(); });
gulp.task('watch', function() { return watch(); });
/*gulp.task('connect', function() {connect.server();});*/


gulp.task('compass', function() {
  return gulp.src(['sass/*.scss', 'sass/*/*.scss'])
    .pipe(compass({
      config_file: 'config.rb',
      css: 'css',
      sass: 'sass'
    }));
});

gulp.task('compass_watch', function() {
  return gulp.src(['sass/*.scss', 'sass/*/*.scss'])
    .pipe(compass({
      config_file: 'config.rb',
      css: 'css',
      sass: 'sass',
      task: 'watch'
    }));
});

// BrowserSynk
gulp.task('browser-sync', function() {
  //watch files
  var files = [
    'css/style.css',
    'templates/*.twig',
    'js/*.js'
  ];

  //initialize browsersync
  browserSync.init(files, {
    //browsersync with a php server
    proxy: "intranet-asp.dev"
  });

  gulp.watch('css/style.css', browserSync.stream);
  gulp.watch(['templates/*.html.twig', 'templates/*/*.html.twig'], browserSync.stream);
  gulp.watch('js/*.js', browserSync.stream);
});

// Default task to be run with `gulp`
gulp.task('default', ['compass_watch', 'browser-sync'], function() {
  browserSync.reload();
});