var Homepage;

(function($) {
  $('.path-frontpage .view-testimonies .view-content').slick({
    dots: true,
    infinite: true,
    autoplay: true,
    speed: 800,
    pauseOnHover: true,
    prevArrow:"<img class='slider-prev-arrow' src='themes/custom/opquast/images/homepage/slider-left.svg'/>",
    nextArrow:"<img class='slider-next-arrow' src='themes/custom/opquast/images/homepage/slider-right.svg'/>"
  });
})(jQuery);