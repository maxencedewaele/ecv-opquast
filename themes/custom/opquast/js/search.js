var Search;

(function($) {
  // Get the modal
  var modal = document.getElementById('search-modal-container');

// Get the button that opens the modal
  var searchToggle = $('.region-header a.icon-pictosearch');
  // var searchSubmit = $('a.icon-pictosearch');

// Get the <span> element that closes the modal
  var span = document.getElementsByClassName("close")[0];

// When the user clicks on the button, open the modal
//   btn.onclick = function(e) {
//
//   }
  searchToggle.click(function(e){
    e.preventDefault();
    modal.style.display = "block";
  })

// When the user clicks on <span> (x), close the modal
  span.onclick = function() {
    modal.style.display = "none";
  }

// When the user clicks anywhere outside of the modal, close it
  window.onclick = function(event) {
    if (event.target == modal) {
      modal.style.display = "none";
    }
  }

  // Submit form on enter key press or search icon click
  $('[id^="search-block-form"] div.icon-pictosearch').click(function(){
    submitSearchForm();
  });
  $('input[id^="edit-keys"]').on("keypress", "*:not(textarea)", function(e) {
    if (e.which == 13) {
      submitSearchForm();
    }
  });

  function submitSearchForm()
  {
    $('form[id^="search-block-form"]').submit();
  }

})(jQuery);