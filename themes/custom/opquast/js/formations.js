var Formations;

(function($) {
  $('.node--type-page-formation .field--name-field-training-one-references .field__items, .node--type-page-formation .field--name-field-training-two-references .field__items').slick({
    // dots: true,
    infinite: true,
    speed: 600,
    autoplay: true,
    prevArrow:"<img class='slider-prev-arrow' src='themes/custom/opquast/images/formations/arrow-left.png'/>",
    nextArrow:"<img class='slider-next-arrow' src='themes/custom/opquast/images/formations/arrow-right.png'/>"
  });
})(jQuery);