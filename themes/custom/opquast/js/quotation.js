var Quotation;

(function($) {
  if ($('body').hasClass('path-devis')){
    // Init form carousel
    $('form.quotation-form').slick({
      dots: false,
      infinite: false,
      speed: 800,
      prevArrow:"<a class='slider-prev-arrow' href='#'>Retour</a>",
      nextArrow:"<a class='slider-next-arrow' href='#'>Suivant</a>"
    });

    // Detect change for radio buttons
    // $('form.quotation-form .step-two input[type="radio"]').on('change', function(){
    //   console.log(this);
    //   $(this).addClass('selected');
    // });
  }
})(jQuery);