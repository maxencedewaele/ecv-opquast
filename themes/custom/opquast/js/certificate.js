var Certificate;

(function($) {
  if ($('.node--type-page-certification').length){
    var $cpt = 0;
    $(document).on('scroll touchmove ready ', function() {
      var target = $('fieldset#target');
      screenTop = $(window).scrollTop(),
        screenBottom = $(window).scrollTop() + window.innerHeight,
        sectionTop = target.position().top,
        sectionBottom = target.position().top + window.innerHeight;

      // Offset so that animation can be seen
      var animationOffset = 200;

      // Triggers the animation
      if (sectionTop < screenBottom - animationOffset && sectionBottom > screenTop - animationOffset && $cpt == 0) {
        setTimeout(function(){
          var agenciesNumber = $('.field--name-field-nb-agencies span.number');
          jQuery({ Counter: 0 }).animate({ Counter: agenciesNumber.text() }, {
            duration: 2000,
            easing: 'swing',
            step: function (i) {
              agenciesNumber.text(Math.ceil(i));
            }
          });
        }, 200);
        setTimeout(function(){
          var certificateNumber = $('.field--name-field-nb-certificte span.number');
          jQuery({ Counter: 0 }).animate({ Counter: certificateNumber.text() }, {
            duration: 2000,
            easing: 'swing',
            step: function (i) {
              certificateNumber.text(Math.ceil(i));
            }
          });
        }, 200);
        setTimeout(function(){
          var schoolsNumber = $('.field--name-field-nb-schools span.number');
          jQuery({ Counter: 0 }).animate({ Counter: schoolsNumber.text() }, {
            duration: 2000,
            easing: 'swing',
            step: function (i) {
              schoolsNumber.text(Math.ceil(i));
            }
          });
        }, 200);

        $cpt++;
      }
    });
  }
})(jQuery);