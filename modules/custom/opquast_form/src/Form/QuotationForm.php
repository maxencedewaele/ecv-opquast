<?php

namespace Drupal\opquast_form\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;

/**
 * Implements an example form.
 */
class QuotationForm extends FormBase {

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'quotation_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $form['nb_candidates'] = [
            '#type' => 'number',
            //'#title' => $this->t('Nombre de candidats'),
            '#prefix' => '<div class="step step-one"><h2>Votre devis est à 3 clics d\'ici</h2><p class="description">Combien d\'employés assisteraient à la formation ?</p><div class="icon-pictoteam gradient"></div>',
            '#suffix' => '<p class="label">Employés</p></div>',
        ];
        $form['nature'] = [
            '#type' => 'radios',
            '#options' => [
                'maitrise' => 'La certification maîtrise de la qualité en projet Web',
                'sensibilisation' => 'La sensibilisation à la qualité Web',
                'referent' => 'La formation référent qualité web',
            ],
            '#prefix' => '<div class="step step-two"><h2>Votre devis est à 2 clics d\'ici</h2><p class="description">Vous êtes intéressés par...</p>',
            '#suffix' => '</div>',
        ];
        //$form['present_formation'] = [
        //    '#type' => 'radios',
        //    '#options' => [
        //        'presential' => 'Formation présentielle',
        //        'online' => 'Formation en ligne',
        //    ],
        //    '#prefix' => '<div class="step step-three"><h2>Votre devis est à 1 clics d\'ici</h2><p class="description">Choix multiple</p>',
        //    '#suffix' => '</div>',
        //];
        $form['present_formation'] = array(
            '#type' => 'checkboxes',
            '#options' => [
                'presential' => 'Formation présentielle',
                'online' => 'Formation en ligne',
            ],
            '#prefix' => '<div class="step step-three"><h2>Votre devis est à 1 clics d\'ici</h2><p class="description">Choix multiple</p>',
            '#suffix' => '</div>',
        );
        $form['name'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Nom'),
            '#prefix' => '<div class="step step-four"><h2>Une dernière chose !</h2><div class="row">',
            '#required' => TRUE,
        ];
        $form['surname'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Prénom'),
            '#suffix' => '</div>',
            '#required' => TRUE,
        ];
        $form['compagny'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Société'),
            '#prefix' => '<div class="row">'
        ];
        $form['email'] = [
            '#type' => 'email',
            '#title' => $this->t('Courriel'),
            '#suffix' => '</div>',
            '#required' => TRUE,
        ];
        $form['phone'] = [
            '#type' => 'tel',
            '#title' => $this->t('Téléphone'),
            '#prefix' => '<div class="row">'
        ];
        $form['address'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Adresse'),
            '#suffix' => '</div><p class="mandatory-fields">* Champs obligatoires</p>',
            '#required' => TRUE,
        ];

        //$form['captcha'] = array(
        //    '#type' => 'captcha',
        //    '#captcha_type' => 'recaptcha/reCAPTCHA',
        //);

        $form['actions']['#type'] = 'actions';
        $form['actions']['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Recevoir mon devis'),
            '#attributes' => ['class' => ['gradient-btn']],
            '#ajax' => array(
                'callback' => '::submitQuotation',
                'wrapper' => 'quotation-form',
            ),
            '#suffix' => '</div>'
        ];


        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        //$response = new AjaxResponse();
        //
        //$errors = $form_state->getErrors();
        //if (array_key_exists('captcha_response', $errors)) {
        //    $response->addCommand(new HtmlCommand("#error-captcha", "Le captcha n'est pas valide. Veuillez réessayer."));
        //    return $response;
        //}
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {

    }

    /**
     * {@inheritdoc}
     */
    public function submitQuotation(array &$form, FormStateInterface $form_state) {
        $response = new AjaxResponse();

        $body_data = array (
            '#theme' => 'quotationEmail',
            '#name' => $form_state->getValue('name'),
            '#email' => $form_state->getValue('email'),
            '#compagny' => $form_state->getValue('compagny'),
            '#phone' => $form_state->getValue('phone'),
            '#address' => $form_state->getValue('address'),
            '#nature' => $form_state->getValue('nature'),
            '#nb_candidates' => $form_state->getValue('nb_candidates'),
            '#remarks' => $form_state->getValue('remarks'),
        );
        $renderEmail = \Drupal::service('renderer')->render($body_data);
        //TODO: use rendered email to send

        $message = '<div class="confirmation"><div class="icon-pictoheart gradient"></div><p class="gradient">Merci !</p><p>Un expert reviendra vers vous dès que possible.</p><p>Un mail vous sera envoyé ou vous serez contacté par téléphone au numéro indiqué précédemment.</p></div>';
        \Drupal::logger('d8mail')->notice($message);
        $response->addCommand(new HtmlCommand('#quotation-form', $message));

        return $response;
    }

}
