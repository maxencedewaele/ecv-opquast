<?php

namespace Drupal\opquast_form\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;

/**
 * Implements a contact form for Opquast
 */
class ContactForm extends FormBase {

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'contact_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $form['name'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Prénom'),
            //'#attributes' => ['class' => 'col-sm']
            '#attributes' => ['class' => ['col-sm']],
            '#required' => TRUE,
            '#prefix' => '<div class="row">'
        ];
        $form['surname'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Nom'),
            '#attributes' => ['class' => ['col-sm']],
            '#required' => TRUE,
            '#suffix' => '</div>'
        ];
        $form['email'] = [
            '#type' => 'email',
            '#title' => $this->t('Courriel'),
            '#attributes' => ['class' => ['col-sm']],
            '#required' => TRUE,
            '#prefix' => '<div class="row">'
        ];
        $form['compagny'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Société'),
            '#attributes' => ['class' => ['col-sm']],
            '#suffix' => '</div>'
        ];
        $form['subject'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Sujet'),
            '#prefix' => '<div class="row">',
            '#suffix' => '</div>'
        ];
        $form['message'] = [
            '#type' => 'textarea',
            '#title' => $this->t('Message'),
            '#prefix' => '<div class="row">',
            '#suffix' => '</div><p class="mandatory-fields">* Champs obligatoires</p>'
        ];
        //$form['captcha'] = array(
        //    '#type' => 'captcha',
        //    '#captcha_type' => 'recaptcha/reCAPTCHA',
        //);

        $form['actions']['#type'] = 'actions';
        $form['actions']['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Envoyer'),
            '#button_type' => 'primary',
            '#attributes' => ['class' => ['gradient-btn']],
            '#ajax' => array(
                'callback' => '::submitContact',
                'wrapper' => 'contact-form',
            ),
        ];
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {
        //$response = new AjaxResponse();
        //
        //$errors = $form_state->getErrors();
        //if (array_key_exists('captcha_response', $errors)) {
        //    $response->addCommand(new HtmlCommand("#error-captcha", "Le captcha n'est pas valide. Veuillez réessayer."));
        //    return $response;
        //}
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {

    }

    /**
     * {@inheritdoc}
     */
    public function submitContact(array &$form, FormStateInterface $form_state) {
        $response = new AjaxResponse();

        $body_data = array (
            '#theme' => 'contactEmail',
            '#name' => $form_state->getValue('name'),
            '#email' => $form_state->getValue('email'),
            '#phone' => $form_state->getValue('phone'),
            '#subject' => $form_state->getValue('subject'),
            '#message' => $form_state->getValue('message'),
        );
        $renderEmail = \Drupal::service('renderer')->render($body_data);
        //TODO: use rendered email to send

        $message = '<div class="confirmation"><div class="icon-pictomessage gradient"></div><p class="gradient">Merci à vous </p><p>Votre message a bien été envoyé</p></div>';
        \Drupal::logger('d8mail')->notice($message);
        $response->addCommand(new HtmlCommand('#contact-form', $message));
        $response->addCommand(new HtmlCommand('#block-maps', ''));
        //TODO: cacher l'autre partie

        return $response;
    }

}
