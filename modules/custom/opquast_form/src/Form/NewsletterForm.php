<?php

namespace Drupal\opquast_form\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements an example form.
 */
class NewsletterForm extends FormBase {

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'newsletter_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $form['email'] = [
            '#type' => 'email',
            //'#value' => 'Demande de devis',
            '#attributes' => ['placeholder' => 'entrez votre e-mail']
        ];

        $form['actions']['#type'] = 'actions';
        $form['actions']['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('OK'),
            '#button_type' => 'primary',
        ];
        return $form;
    }

    ///**
    // * {@inheritdoc}
    // */
    //public function validateForm(array &$form, FormStateInterface $form_state) {
    //    if (strlen($form_state->getValue('phone_number')) < 3) {
    //        $form_state->setErrorByName('phone_number', $this->t('The phone number is too short. Please enter a full phone number.'));
    //    }
    //}

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        drupal_set_message('Soumission du formulaire de Newsletter.');
    }

}
