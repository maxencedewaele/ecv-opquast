<?php

namespace Drupal\opquast_form\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'newsletter_block' Block.
 *
 * @Block(
 *   id = "newsletter_block",
 *   admin_label = @Translation("Formulaire d'inscription à la Newsletter"),
 *   category = @Translation("Formulaire d'inscription à la Newsletter"),
 * )
 */
class NewsletterBlock extends BlockBase {

    /**
     * {@inheritdoc}
     */
    public function build() {
        $form = \Drupal::formBuilder()->getForm('Drupal\opquast_form\Form\NewsletterForm');

        return array(
            '#theme' => 'newsletterBlock',
            '#form' => $form
        );
    }

}