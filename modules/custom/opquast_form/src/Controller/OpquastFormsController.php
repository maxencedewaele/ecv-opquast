<?php

namespace Drupal\opquast_form\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Provides a controller which displays Opquast's forms.
 */
class OpquastFormsController extends ControllerBase {

    /**
     * @return array
     */
    public function contactPage()
    {
        return [
            '#theme' => 'opquastForms',
        ];
    }

    /**
     * @return array
     */
    public function contactForm()
    {
        $form = \Drupal::formBuilder()->getForm('Drupal\opquast_form\Form\ContactForm');

        return [
            '#theme' => 'contactForm',
            '#form' => $form
        ];
    }

    /**
     * @return array
     */
    public function quotationForm()
    {
        $form = \Drupal::formBuilder()->getForm('Drupal\opquast_form\Form\QuotationForm');

        return [
            '#theme' => 'quotationForm',
            '#form' => $form
        ];
    }
}
