<?php

namespace Drupal\opquast_certificates\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\opquast_certificates\CertificateListStorage;

/**
 * Provides a 'certificates_list' Block.
 *
 * @Block(
 *   id = "certificates_list",
 *   admin_label = @Translation("Liste des certificats pour chaque partenaire"),
 *   category = @Translation("Certificats"),
 * )
 */
class CertificatesList extends BlockBase {

    /**
     * {@inheritdoc}
     */
    public function build() {
        $certificates = null;
        $node = \Drupal::routeMatch()->getParameter('node');
        if ($node instanceof \Drupal\node\NodeInterface) {
            // You can get nid and anything else you need from the node object.
            $nid = $node->id();
            $certificateListStorage = new CertificateListStorage();
            $certificates = $certificateListStorage->getListContent($nid);
        }

        return array(
            '#theme' => 'certificatesList',
            '#certificates' => $certificates,
            '#cache' => [
                'max-age' => 0,
            ],
        );
    }

}