<?php
/**
 * @file
 * Contains \Drupal\opquast_certificates\CertificateListStorage.
 */
namespace Drupal\opquast_certificates;

use Drupal\Core\Database\Database;

class CertificateListStorage {

    /**
     * Gets certificate's list content
     *
     * @param $partnerId
     *
     * @return mixed
     */
    public function getListContent($partnerId){
        $query = \Drupal::entityQuery('node')
            ->condition('type', 'certificate')
            ->condition('field_partner', $partnerId)
            ->condition('status', '1');

        $result = $query->execute();
        $nodes = \Drupal\node\Entity\Node::loadMultiple($result);

        $build = \Drupal::entityTypeManager()->getViewBuilder('node')->viewMultiple($nodes, 'teaser');

        return $build;
    }
}