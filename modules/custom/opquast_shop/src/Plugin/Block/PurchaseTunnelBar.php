<?php

namespace Drupal\opquast_shop\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'purchase_progress_bar' Block.
 *
 * @Block(
 *   id = "purchase_progress_bar",
 *   admin_label = @Translation("Barre de progression du tunnel d'achat"),
 *   category = @Translation("Barre de progression du tunnel d'achat"),
 * )
 */
class PurchaseTunnelBar extends BlockBase {

    /**
     * {@inheritdoc}
     */
    public function build() {
        $routeName = \Drupal::routeMatch()->getRouteName();
        $step = "";

        switch ($routeName) {
            case "commerce_cart.page":
                $step = 1;
                break;
            case "commerce_checkout.form":
                $step = 2;
                break;
        }

        return array(
            '#theme' => 'purchaseProgressBar',
            '#step' => $step,
            '#cache' => [
                'max-age' => 0,
            ],
        );
    }

}